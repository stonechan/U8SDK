package com.u8.sdk;

import org.json.JSONObject;

/***
 * 登录成功后的返回信息
 * 
 * @author xiaohei
 * 
 */
public class LoginResult {

	private String gameID; // 游戏id
	private String sid; // userId
	private String channel; // 渠道号
	private String sdkversion = "";
	private String version = "1";
	private String expansion = ""; // 扩展,各个渠道可能不一样（uid）
	private String uid = ""; // uid 不一定每个渠道都有
	private String expand = ""; // 扩展,各个渠道可能不一样
	private boolean isSDKExit = false; // 是否实现SDK的退出确认界面

	public LoginResult(String sid, String channel, String expansion,
			String gameID, String sdkVersion, String version) {
		this.sid = sid;
		this.channel = channel;
		this.expansion = expansion;
		this.sdkversion = sdkVersion;
		this.version = version;
		this.gameID = gameID;
		this.uid = expansion;
		this.expand = version;
	}

	public String toJsonString() {
		try {
			JSONObject json = new JSONObject();
			json.put("sid", this.sid);
			json.put("channel", this.channel);
			json.put("expansion", expansion);
			json.put("sdkversion", sdkversion);
			json.put("version", version);
			json.put("gameID", gameID);
			json.put("uid", uid);
			json.put("expand", expand);
			return json.toString();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "";
	}

	public boolean isSDKExit() {
		return isSDKExit;
	}

	public void setSDKExit(boolean isSDKExit) {
		this.isSDKExit = isSDKExit;
	}

	public String getSid() {
		return sid;
	}

	public void setSid(String sid) {
		this.sid = sid;
	}

	public String getExpansion() {
		return expansion;
	}

	public void setExpansion(String expansion) {
		this.expansion = expansion;
	}

	public String getChannel() {
		return channel;
	}

	public void setChannel(String channel) {
		this.channel = channel;
	}

	public String getSdkversion() {
		return sdkversion;
	}

	public void setSdkversion(String sdkversion) {
		this.sdkversion = sdkversion;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public String getGameID() {
		return gameID;
	}

	public String getUid() {
		return uid;
	}

	public String getExpand() {
		return expand;
	}

}
