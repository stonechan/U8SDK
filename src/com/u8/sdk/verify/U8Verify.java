package com.u8.sdk.verify;

import java.util.HashMap;
import java.util.Map;

import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

import com.u8.sdk.LoginResult;
import com.u8.sdk.U8SDK;
import com.u8.sdk.utils.EncryptUtils;
import com.u8.sdk.utils.U8HttpUtils;

public class U8Verify{

	private static final String AUTH_URL = "http://218.245.3.130:8080/Sdk_Login.php";
	
	/***
	 * 访问U8Server验证sid的合法性，同时获取U8Server返回的token，userID,sdkUserID信息
	 * @param result
	 * @return
	 */
	public static UToken auth(LoginResult result){
		
		try{
			Map<String, String> params = new HashMap<String, String>();
			params.put("appID", U8SDK.getInstance().getAppID()+"");
			params.put("channelID", result.getChannel());
			params.put("sid", result.getSid());
			params.put("extension", result.getExpansion());
			
            StringBuilder sb = new StringBuilder();
            sb.append("appID=").append(U8SDK.getInstance().getAppID()+"")
                    .append("channelID=").append(result.getChannel())
                    .append("sid=").append(result.getSid()).append(U8SDK.getInstance().getAppKey());			
			
            String sign = EncryptUtils.md5(sb.toString()).toLowerCase();
            
            params.put("sign", sign);
            
			String authResult = U8HttpUtils.httpGet(AUTH_URL, params);
			
			Log.d("U8SDK", "The sign is " + sign + "The auth result is "+authResult);
			
			return parseAuthResult(authResult);
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return new UToken();
		
	}
	
	
	private static UToken parseAuthResult(String authResult){
		
		try {
			JSONObject jsonObj = new JSONObject(authResult);
			int state = jsonObj.getInt("state");
			
			if(state != 1){
				Log.d("U8SDK", "auth failed. the state is "+ state);
				return new UToken();
			}
			
			JSONObject jsonData = jsonObj.getJSONObject("data");
			
			return new UToken(jsonData.getInt("userID")
					, jsonData.getString("sdkUserID")
					, jsonData.getString("token")
					, jsonData.getString("extension"));
			
		} catch (JSONException e) {

			e.printStackTrace();
		}
		
		return new UToken();
	}
}
