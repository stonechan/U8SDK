package com.u8.sdk.verify;

public class UToken {

	private boolean suc;
	private int userID;
	private String sdkUserID;
	private String token;
	private String extension;
	
	public UToken(){
		this.suc = false;
	}
	
	public UToken(int userID, String sdkUserID, String token, String extension){
		this.userID = userID;
		this.sdkUserID = sdkUserID;
		this.token = token;
		this.extension = extension;
		this.suc = true;
	}
	
	public int getUserID() {
		return userID;
	}
	public void setUserID(int userID) {
		this.userID = userID;
	}
	public String getSdkUserID() {
		return sdkUserID;
	}
	public void setSdkUserID(String sdkUserID) {
		this.sdkUserID = sdkUserID;
	}
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}

	public boolean isSuc() {
		return suc;
	}

	public void setSuc(boolean suc) {
		this.suc = suc;
	}

	public String getExtension() {
		return extension;
	}

	public void setExtension(String extension) {
		this.extension = extension;
	}
	
	
}
