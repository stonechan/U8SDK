package com.u8.sdk;

import android.content.Context;
import android.content.res.Resources;
import android.util.Log;

public class UniR {

    private static final String DRAWABLE = "drawable";
    private static final String LAYOUT = "layout";
    private static final String ID = "id";
    private static final String COLOR = "color";
    private static final String DIMEN = "dimen";
    private static final String STRING = "string";
    private static final String STYLE = "style";
    private static final String RAW = "raw";

    private static int getResourcesIdByName(String defType, String name) {
    	Context context = U8SDK.getInstance().getContext();
        Resources resources = context.getResources();
        int id = resources.getIdentifier(name, defType, context.getPackageName());
        if(id == 0) {
        	Log.e("SDK", "找不到"+defType+"."+name);
        }
        return id;
    }

    public static int getDrawableId(String drawableName) {
        return getResourcesIdByName(DRAWABLE, drawableName);
    }

    public static int getLayoutId(String layoutName) {
        return getResourcesIdByName(LAYOUT, layoutName);
    }

    public static int getViewID(String viewId) {
        return getResourcesIdByName(ID, viewId);
    }

    public static int getColorId(String colorName) {
        return getResourcesIdByName(COLOR, colorName);
    }

    public static int getDimenId(String dimenName) {
        return getResourcesIdByName(DIMEN, dimenName);
    }

    public static int getStringId(String stringName) {
        return getResourcesIdByName(STRING, stringName);
    }

    public static int getStyleId(String styleName) {
        return getResourcesIdByName(STYLE, styleName);
    }

    public static int getRawId(String rawName) {
        return getResourcesIdByName(RAW, rawName);
    }

}
