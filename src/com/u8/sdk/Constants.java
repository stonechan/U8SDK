package com.u8.sdk;

public class Constants {

	public static final int PLUGIN_TYPE_USER = 1;			//用户登录等插件
	public static final int PLUGIN_TYPE_PAY = 2;			//支付插件
	public static final int PLUGIN_TYPE_PUSH = 3;			//推送插件
	public static final int PLUGIN_TYPE_SHARE = 4;			//分享插件
	public static final int PLUGIN_TYPE_UPDATE = 5;			//自动更新插件
	
}
