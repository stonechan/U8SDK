package com.u8.sdk;


public interface IU8SDKListener {
	
	public void onResult(int code, String msg);
	
	public void onInitResult(InitResult result);
	
	public void onLoginResult(LoginResult result);
	
	public void onSwitchAccount();
	
	public void onSwitchAccount(LoginResult result);
	
	public void onLogout();
	
	public void onAuthResult(boolean suc, int userID, String sdkUserID, String token);
	
	public void onPayResult(PayResult result);
}
