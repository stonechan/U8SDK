package com.u8.sdk.plugin;

import java.util.HashMap;
import java.util.Map;

import com.u8.sdk.IUser;
import com.u8.sdk.PluginFactory;
import com.u8.sdk.UserExtraData;

/**
 * 用户插件
 * @author xiaohei
 *
 */
public class U8User{
	private static U8User instance;
	
	private IUser userPlugin;
	
	private Map<String,Boolean> boolMap = new HashMap<String,Boolean>(); 
	
	private U8User(){
		
	}
	
	public void init(){
		this.userPlugin = (IUser)PluginFactory.getInstance().initPlugin(IUser.PLUGIN_TYPE);
	}
	
	public static U8User getInstance(){
		if(instance == null){
			instance = new U8User();
		}
		
		return instance;
	}
	
	public void setSupport(String feature,boolean value){
		boolMap.put(feature, value);
	}
	
	public boolean isSupport(String feature){
		
		if (boolMap.get(feature) == null){
			return false;
		}
		return boolMap.get(feature);
	}
	
	public void showAccountCenter(){
		if(userPlugin==null){
			return;
		}
		userPlugin.showAccountCenter();
	}
	
	/**
	 * 登录接口
	 */
	public void login(){
		if(userPlugin==null){
			return;
		}
		
		userPlugin.login();
	}
	
	public void login(String customData){
		if(userPlugin == null){
			return;
		}
		userPlugin.login(customData);
	}
	
	public void switchLogin() {
		if(userPlugin == null){
			return;
		}
		userPlugin.switchLogin();
	}
	
	/**
	 * 退出当前帐号
	 */
	public void logout() {
		if (userPlugin == null) {
			return;
		}
		
		userPlugin.logout();
	}
	
	/***
	 * 提交扩展数据，角色登录成功之后，需要调用
	 * @param extraData
	 */
	public void submitExtraData(UserExtraData extraData){
		if(this.userPlugin == null){
			return;
		}
		userPlugin.submitExtraData(extraData);
	}
	
	/**
	 * SDK退出接口，有的SDK需要在退出的时候，弹出SDK的退出确认界面。
	 * 如果SDK不需要退出确认界面，则弹出游戏自己的退出确认界面
	 */
	public void exit(){
		if(this.userPlugin == null){
			return;
		}
		userPlugin.exit();
	}
}
